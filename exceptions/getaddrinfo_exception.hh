#include "./socket_exception.hh"

namespace socklib {
    class getaddrinfo_exception : socklib::socket_exception
    {
		public:
        getaddrinfo_exception(const std::string& message)
            : socket_exception(message, "getaddrinfo")
        {}
    };
}
