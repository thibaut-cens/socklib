#pragma once

#include <exception>
#include <string>
#include <sstream>

namespace socklib {
    class socket_exception : std::exception
    {
    private:
        std::string _message;
        std::string _prefix;

    public:
        socket_exception(
            const std::string& message, const std::string& prefix = "")
            : _message(message)
            , _prefix(prefix)
        {}

		const char* what() const noexcept override {
			std::stringstream output{};

			if (!_prefix.empty())
				output << _prefix << ": ";
			output << _message;
			return output.str().c_str();
		}
    };
}
