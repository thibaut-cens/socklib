#pragma once

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <optional>
#include <ostream>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include "./exceptions/getaddrinfo_exception.hh"
#include "./exceptions/socket_exception.hh"
#include "./socket_status.hh"

namespace socklib
{
    class Socket
    {
      protected:
        struct addrinfo *_info = nullptr;
        std::string _socket_name;
        int _sockfd;
        SocketStatus _status;
        int _errno = 0;

      public:
        Socket(const std::string &port, const std::string &address,
               const struct addrinfo &hints,
               const std::string &socket_name = "socket")
            : _socket_name(socket_name)
            , _status(SocketStatus::CLOSED)
        {
            int status;
            if ((status =
                     getaddrinfo((address.empty() ? nullptr : address.c_str()),
                                 port.c_str(), &hints, &_info))
                != 0)
            {
                throw getaddrinfo_exception(gai_strerror(status));
            }

            open();
        }

        Socket(const int &sockfd, const std::string &name = "accept_socket")
            : _sockfd(sockfd)
            , _socket_name(name)
            , _status(SocketStatus::ACCEPT)
        {
        }

        ~Socket()
        {
            freeaddrinfo(_info);
            close();
        }

        bool open()
        {
            if ((_sockfd = socket(_info->ai_family, _info->ai_socktype,
                                  _info->ai_protocol))
                != -1)
                _status = SocketStatus::OPEN;
            else
                _errno = errno;
            return _status == SocketStatus::OPEN;
        }

        bool is_open() const
        {
            return _status == SocketStatus::OPEN;
        }

        int close()
        {
            int ret;
            if ((ret = ::close(_sockfd)) == 0)
            {
                _status = SocketStatus::CLOSED;
                _sockfd = 0;
            }
            else
                _errno = errno;
            return ret;
        };

        int bind()
        {
            int ret;
            if ((ret = ::bind(_sockfd, _info->ai_addr, _info->ai_addrlen)) != 0)
                _status = SocketStatus::BOUND;
            else
                _errno = errno;
            return ret;
        }

        int connect()
        {
            int ret;
            if ((ret = ::connect(_sockfd, _info->ai_addr, _info->ai_addrlen))
                == 0)
                _status = SocketStatus::CONNECTED;
            else
                _errno = errno;
            return ret;
        }

        int listen(const int &backlog = 5)
        {
            int ret;
            if ((ret = ::listen(_sockfd, backlog)) == 0)
                _status = SocketStatus::LISTENING;
            else
                _errno = errno;
            return ret;
        }

        int accept()
        {
            int new_sockfd;
            if ((new_sockfd =
                     ::accept(_sockfd, _info->ai_addr, &(_info->ai_addrlen)))
                == -1)
                _errno = errno;
            return new_sockfd;
        }

        int send(const void *msg, int len, int flags = 0)
        {
            int ret;
            if ((ret = ::send(_sockfd, msg, len, flags)) != 0)
                _errno = errno;
            return ret;
        }

        int recv(void *buffer, int len, int flags = 0)
        {
            int ret;
            if ((ret = ::recv(_sockfd, buffer, len, flags)) != 0)
                _errno = errno;
            return ret;
        }

        std::string recv(int len, int flags = 0)
        {
            char buffer[len];
            int ret;
            if ((ret = ::recv(_sockfd, buffer, len, flags)) != 0)
                _errno = errno;

            return std::string(buffer);
        }

        friend std::ostream &operator<<(std::ostream &os, const Socket &s)
        {
            os << '[' << s._socket_name << " "
               << get_ip_str(s._info->ai_addr, s._info->ai_addrlen) << ']';
            return os;
        }

        static struct addrinfo get_hint()
        {
            return {};
        };

        static std::string get_ip_str(const struct sockaddr *address,
                                      const socklen_t &max_size)
        {
            char s[max_size];
            std::string port;
            switch (address->sa_family)
            {
                case AF_INET:
                {
                    auto sa =
                        (reinterpret_cast<const struct sockaddr_in *>(address));
                    inet_ntop(address->sa_family, &(sa->sin_addr), s, max_size);
                    port = std::to_string(ntohs(sa->sin_port));
                    break;
                }

                case AF_INET6:
                {
                    auto sa = (reinterpret_cast<const struct sockaddr_in6 *>(
                        address));
                    inet_ntop(address->sa_family, &(sa->sin6_addr), s,
                              max_size);
                    port = std::to_string(ntohs(sa->sin6_port));
                    break;
                }
                default:
                    return "Unknown AF(" + std::to_string(address->sa_family)
                           + ")";
            }
            return std::string(s) + ":" + port;
        }

        const std::string &getSocketName() const
        {
            return _socket_name;
        }

        int getSockfd() const
        {
            return _sockfd;
        }

        int getErrno() const
        {
            return _errno;
        }

        void setSocketName(const std::string &socketName)
        {
            _socket_name = socketName;
        }
    };
} // namespace socklib
