#pragma once

enum SocketStatus {
    CLOSED,
    OPEN,
    BOUND,
    CONNECTED,
    LISTENING,
    ACCEPT
};