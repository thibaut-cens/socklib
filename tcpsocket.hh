#pragma once

#include "./socket.hh"

namespace socklib {
    class TcpSocket : public Socket
    {
    public:
        TcpSocket(const std::string& port, const std::string& address = "",
            const std::string& socket_name = "tcp_socket")
            : Socket(port, address, get_hint(), socket_name)
        {}

        static struct addrinfo get_hint()
        {
            struct addrinfo hint {};
            hint.ai_family = AF_UNSPEC;
            hint.ai_flags = AI_PASSIVE;
            hint.ai_socktype = SOCK_STREAM;
            return hint;
        }
    };
}