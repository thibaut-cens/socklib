#pragma once

#include "./socket.hh"

namespace socklib {
    class UdpSocket : public Socket
    {
    public:
        UdpSocket(const std::string& port, const std::string& address = "",
            const std::string& socket_name = "udp_socket")
            : Socket(port, address, get_hint(), socket_name)
        {}

        static struct addrinfo get_hint()
        {
            struct addrinfo hint {};
            hint.ai_family = AF_UNSPEC;
            hint.ai_flags = SOCK_DGRAM;
            hint.ai_socktype = SOCK_STREAM;
            return hint;
        }
    };
}